"use strict";

const Router = require("koa-router");
const router = new Router();
const { testCtr } = require("../controller");

router.prefix('/test')

router.post("/save", testCtr.save);
router.get("/totalByRole", testCtr.totalByRole);
router.post("/memberList", testCtr.list);
router.post("/memberInfo", testCtr.searchInfo);
router.get("/getStoreByRole", testCtr.getStoreByRole);
router.post("/getStoreById", testCtr.getDeptInfoById);
router.post("/delete", testCtr.delete);
router.get("/myStores", testCtr.getMyDeptList);
router.get("/___test", testCtr.test);

module.exports = router;
