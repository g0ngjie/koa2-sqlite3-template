"use strict";

const test = require('./test')

module.exports = (app) => {
  [test].forEach(router => {
    app.use(router.routes(), router.allowedMethods())
  })
};
