"use strict";

const Sequelize = require('sequelize');
const DB = new Sequelize({
  host: 'localhost',
  dialect: 'sqlite',

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  storage: './data.db',
  operatorsAliases: false
});

module.exports = DB;
