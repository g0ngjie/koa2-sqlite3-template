"use strict";

const { memProxy } = require("../proxy");
const { KEnum, KUtil, KError, KLayout } = require("../utils");
const _ = require("lodash");

/**
 */
exports.save = async (ctx, next) => {
  const { body, user } = ctx.request;
  throw new Error("哈哈哈a")
  await KLayout.layout(ctx, 'result');
  await next();
};

/**
 */
exports.totalByRole = async (ctx, next) => {
  await KLayout.layout(ctx, 'result');
  await next();
};

/**
 */
exports.list = async (ctx, next) => {

  await KLayout.layout(ctx, '');
  await next();
};

/**
 */
exports.searchInfo = async (ctx, next) => {

  await KLayout.layout(ctx, 'result');
  await next();
};

/**
 */
exports.getStoreByRole = async (ctx, next) => {

  await KLayout.layout(ctx, 'result');
  await next();
};

/**
 */
exports.getDeptInfoById = async (ctx, next) => {
  const { deptId } = ctx.request.body;
  await KLayout.layout(ctx, 'deptInfo');
  await next();
};

/**
 */
exports.delete = async (ctx, next) => {
  const { id } = ctx.request.body;
  await KLayout.layout(ctx, 'result');
  await next();
};

/**
 */
exports.getMyDeptList = async (ctx, next) => {
  const { user } = ctx.request;
  await KLayout.layout(ctx, 'deptList');
  await next();
};

/**
 */
exports.test = async (ctx, next) => {
  await KLayout.layout(ctx, 'result');
  await next();
};
