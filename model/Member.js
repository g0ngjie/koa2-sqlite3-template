"use strict";

const Sequelize = require("sequelize");
const Sqlite = require("../sqlite/index");

/**
 * @class MemberModel
 * @property {String} mobile 手机号
 * @property {String} name 用户
 * @property {Number|KEnum.Gender} gender 性别 1女2男
 * @property {String} store 门店
 * @property {String} remark 评论
 * @property {String} imgUrl 图片列表
 * @property {String} userId 用户id
 * @property {String} deptId 部门id
 */
const MemberModel = Sqlite.define("member", {
  mobile: Sequelize.STRING,
  name: Sequelize.STRING,
  gender: Sequelize.INTEGER,
  store: Sequelize.STRING,
  remark: Sequelize.STRING,
  imgUrl: Sequelize.TEXT,
  userId: Sequelize.STRING,
  deptId: Sequelize.STRING,
});

MemberModel.sync();
module.exports = MemberModel;
