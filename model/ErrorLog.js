"use strict";

const Sequelize = require("sequelize");
const Sqlite = require("../sqlite/index");

/**
 * @class ErrorLogModel
 * @property {String} url
 * @property {String} query
 * @property {String} requestBody
 * @property {String} method
 * @property {String} requestIP
 * @property {String} responseIP
 * @property {String} responseError
 */
const ErrorLogModel = Sqlite.define("errorlog", {
  url: Sequelize.STRING,
  query: Sequelize.STRING,
  requestBody: Sequelize.STRING,
  method: Sequelize.STRING,
  requestIP: Sequelize.STRING,
  responseIP: Sequelize.STRING,
  responseError: Sequelize.TEXT,
});

ErrorLogModel.sync();
module.exports = ErrorLogModel;
